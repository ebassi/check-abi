# check-abi

check-abi is a script to build and compare the ABIs of the libraries
built by 2 different git revisions.

Typical use is in a CI script that would compare the current HEAD with
an older revision to make sure that unintended ABI differences are
not introduced.

```sh
$ export LAST_ABI_BREAK=7cc6250695ca7c7f326f0dd1dcbabc0061123b6b
$ check-abi $LAST_ABI_BREAK $(git rev-parse HEAD)
```

By default all the `*.so` files installed in the target `/usr/lib`
directory are individually checked for ABI differences.

You can pass additional build parameters (`prefix` and `libdir` arguments
are always passed) to enable or disable compile-time features. This allows
checking for ABI consistency between two variants, for example:
```sh
$ check-abi --old-parameters="-Doption=disabled" --new-parameters="-Doption=enabled" $LAST_ABI_BREAK $(git rev-parse HEAD)
```

If you need to quiet changes to particular functions, variables, structs,
or types, you can pass `--suppr` which will expect a
[libabigail suppression file](https://www.sourceware.org/libabigail/manual/libabigail-concepts.html#suppression-specifications).

## Installation

`check-abi` uses [meson](https://mesonbuild.com/) as its build system.
You will also need to have the `abidiff` utility installed, which is
available in the `libabigail` package in Fedora.

If you're feeling lucky, you might be able to use one of the `curl | bash`
scripts in the `contrib/` directory.
